app.controller('meetupsController', ['$scope', '$resource', function( $scope, $resource ) {

	var Meetup = $resource('/api/meetups');

	$scope.meetups = [];

	Meetup.query(function(results) {
		$scope.meetups = results;
	})
	

	$scope.createMeetup = function() {

		var meetup = new Meetup();

		meetup.name = $scope.meetupName;
		meetup.date_time = $scope.meetupDate;
		meetup.location = $scope.meetupLocation;

		meetup.$save(function(result) {
			$scope.meetups.push(result);
		});

		/*$scope.meetups.push({name: $scope.meetupName, date_time: $scope.meetupDate, location: $scope.meetupLocation});*/
		$scope.meetupName = "";
		$scope.meetupDate = "";
		$scope.meetupLocation = "";

	}

	

}])