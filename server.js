var express = require('express'),
app = express(),
bodyParser = require('body-parser'),
mongoose = require('mongoose'),
meetupsController = require('./server/controllers/meetups-controller');

//app.use(bodyParser());

mongoose.connect('mongodb://localhost:27017/mean-tut');

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/client/views/index.html');
});

app.get('/api/meetups', meetupsController.list);

app.use('/js', express.static(__dirname + '/client/js'));

app.post('/api/meetups', meetupsController.create)

app.listen(3000, function() {
	console.log('I\'m listening...');
})