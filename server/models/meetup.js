var mongoose = require('mongoose');

module.exports = mongoose.model('Meetup', {
	name: String,
	location: String,
	date_time: String
})